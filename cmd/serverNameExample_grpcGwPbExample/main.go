// Package main is the grpc gateway server of the application.
package main

import (
	"gitee.com/yzsunjianguo/sponge/cmd/serverNameExample_grpcGwPbExample/initial"

	"gitee.com/yzsunjianguo/sponge/pkg/app"
)

func main() {
	initial.InitApp()
	services := initial.CreateServices()
	closes := initial.Close(services)

	a := app.New(services, closes)
	a.Run()
}
