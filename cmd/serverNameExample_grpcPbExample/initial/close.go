package initial

import (
	"context"
	"time"

	"gitee.com/yzsunjianguo/sponge/internal/config"
	//"gitee.com/yzsunjianguo/sponge/internal/model"

	"gitee.com/yzsunjianguo/sponge/pkg/app"
	"gitee.com/yzsunjianguo/sponge/pkg/tracer"
)

// Close releasing resources after service exit
func Close(servers []app.IServer) []app.Close {
	var closes []app.Close

	// close server
	for _, s := range servers {
		closes = append(closes, s.Stop)
	}

	// close mysql
	//closes = append(closes, func() error {
	//	return model.CloseMysql()
	//})

	// close redis
	//if config.Get().App.CacheType == "redis" {
	//	closes = append(closes, func() error {
	//		return model.CloseRedis()
	//	})
	//}

	// close tracing
	if config.Get().App.EnableTrace {
		closes = append(closes, func() error {
			ctx, _ := context.WithTimeout(context.Background(), 2*time.Second) //nolint
			return tracer.Close(ctx)
		})
	}

	return closes
}
