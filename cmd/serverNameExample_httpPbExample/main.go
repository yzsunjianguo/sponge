// Package main is the http server of the application.
package main

import (
	"gitee.com/yzsunjianguo/sponge/cmd/serverNameExample_httpPbExample/initial"

	"gitee.com/yzsunjianguo/sponge/pkg/app"
)

func main() {
	initial.InitApp()
	services := initial.CreateServices()
	closes := initial.Close(services)

	a := app.New(services, closes)
	a.Run()
}
