### Examples of use

- [1_web-gin-CRUD](https://gitee.com/yzsunjianguo/sponge_examples/tree/main/1_web-gin-CRUD)
- [2_web-gin-protobuf](https://gitee.com/yzsunjianguo/sponge_examples/tree/main/2_web-gin-protobuf)
- [3_micro-grpc-CRUD](https://gitee.com/yzsunjianguo/sponge_examples/tree/main/3_micro-grpc-CRUD)
- [4_micro-grpc-protobuf](https://gitee.com/yzsunjianguo/sponge_examples/tree/main/4_micro-grpc-protobuf)
- [5_micro-gin-rpc-gateway](https://gitee.com/yzsunjianguo/sponge_examples/tree/main/5_micro-gin-rpc-gateway)
- [6_micro-cluster](https://gitee.com/yzsunjianguo/sponge_examples/tree/main/6_micro-cluster)
- [7_community-single](https://gitee.com/yzsunjianguo/sponge_examples/tree/main/7_community-single)
- [8_community-cluster](https://gitee.com/yzsunjianguo/sponge_examples/tree/main/8_community-cluster)
- [9_order-grpc-distributed-transaction](https://gitee.com/yzsunjianguo/sponge_examples/tree/main/9_order-grpc-distributed-transaction)
