package excel_tool

import (
	"fmt"
	"gitee.com/yzsunjianguo/sponge/pkg/snowflake"
	"testing"

	"github.com/gookit/goutil/dump"
)

func TestExcel(t *testing.T) {
	headers := []string{"用户名", "性别", "年龄"}
	values := [][]interface{}{
		{"测试", "1", "男"},
		{
			"ggr1", "1", "男",
		},
	}
	f, err := ExportExcel("sheet1", headers, values)
	defer func(f *excelize.File) {
		err := f.Close()
		if err != nil {

		}
	}(f)
	if err != nil {
		dump.P(err)
	}
	err = snowflake.Init(1)
	if err != nil {
		dump.P(err)
	}
	err = f.SaveAs("./static/export/" + fmt.Sprintf("%d", snowflake.NewID()) + "test1.xlsx")
	if err != nil {
		dump.P(err.Error())
	}
}
