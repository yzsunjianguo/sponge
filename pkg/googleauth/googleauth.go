package googleauth

import (
	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
)

// Sample in-memory storage for secret keys (replace with a proper storage solution in a real-world scenario)

var Users = make(map[string]string)

// GenerateTOTPSecret generates a new TOTP secret key.
func GenerateTOTPSecret(accountName string) (*otp.Key, error) {
	return totp.Generate(totp.GenerateOpts{
		Issuer:      "shop",
		AccountName: accountName,
	})
}

// GetProvisioningURI generates a provisioning URI for the QR code.
func GetProvisioningURI(key *otp.Key) string {
	return key.URL()
}

// ValidateTOTPToken validates a TOTP token against the provided secret.
func ValidateTOTPToken(token, secret string) bool {
	return totp.Validate(token, secret)
}
